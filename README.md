<h1 align="center">
TRAINING AN EMBEDDED OBJECT DETECTOR FOR INDUSTRIAL SETTINGS WITHOUT REAL IMAGES
</h1>
<div align="center">
<h3>
<a href="https://liris.cnrs.fr/page-membre/julia-cohen">Julia Cohen</a>,
<a href="https://liris.cnrs.fr/page-membre/carlos-crispim-junior">Carlos Crispim-Junior</a>,
<a>Jean-Marc Chiappa</a>,
<a href="https://liris.cnrs.fr/page-membre/laure-tougne">Laure Tougne</a>
<br>
<br>
IEEE ICIP: International Conference on Image Processing, 2021
</h3>
</div>


# Table of content
- [Overview](#overview)
- [Installation](#installation)
- [Dataset](#dataset)
- [Evaluation](#evaluation)
- [Training](#training)
- [Citation](#citation)
- [Acknowledgements](#acknowledgements)

# Overview
This repository contains the code for MobileNet-V3 SSD, MobileNet-V3 Small SSD, and MobileNet-V2 SSD 
as described in the paper. The weights of the models trained on the T-LESS dataset are available 
for download, as well as the compared Mask R-CNN model.

![Results Images](data/images/results.png)

# Installation
## Project
Clone the current repository:
```
git clone --recurse-submodules https://gitlab.liris.cnrs.fr/jcohen/synthetic-ssd.git
```
The "Object Detection Metrics" project should be downloaded in the `deps` folder. 
After download, move the `lib` subfolder and `_init_paths.py` file into `synthetic_ssd\metrics`.

## Environment
Create and activate a conda environmen: 
```
conda create --file requirements.yml
```
or create a virtual environment and install packages with pip:
```
pip install -r requirements.txt
```

This project has been tested on Windows and Linux with Python 3.8 and PyTorch 1.9, but previous versions of
Python 3 and PyTorch should also work.

# Dataset
The T-LESS dataset is available on the [BOP website](https://bop.felk.cvut.cz/datasets/). 
We use the "PBR-BlenderProc4BOP training images" and "All test images" subsets, saved in the `data` folder. 
The expected folder structure is:  
```
- data
| - tless
  | - test_primesense
  | - train_pbr
```
Otherwise, you can change the `TLESS_BASE_PATH` variable in
[synthetic_ssd\config.py](synthetic_ssd/config.py)

# Evaluation
For the Mask R-CNN model, download the trained weights file from the 
[CosyPose project](https://github.com/ylabbe/cosypose#bop20-models-and-results) into the `data/weights` 
folder, or elsewhere and change the `MASK_RCNN_PATH` variable in [synthetic_ssd\config.py](synthetic_ssd/config.py). The
model evaluated in the paper corresponds to the one with `model_id` detector-bop-tless-pbr--873074.

To reproduce the performance reported in the paper, use the evaluation script:
```
python -m synthetic_ssd.scripts.run_eval \
--model [mobilenet_v2_ssd, mobilenet_v3_ssd, mobilenet_v3_small_ssd, mask_rcnn] \
--tf [aug1, aug2]
```
Default values are set to **mobilenet_v2_ssd** and **aug2**.

Model   | mAP (%) | Parameters (M)
:--- | :----: | :----:
Mask R-CNN | 32.8 | 44.0
V3small-SSD (aug1) | 18.6 | 2.6
V3-SSD (aug1) | 36.3 | 4.9 
V2-SSD (aug1) | 38.3 | 3.5
V3small-SSD (aug2) | 23.5 | 2.6
V3-SSD (aug2) | 46.1 | 4.9
**V2-SSD (aug2)** | **47.7** | 3.5


# Training
To train one of the SSD models, use the training script:
```
python -m synthetic_ssd.scripts.train_ssd \
  -h, --help            show this help message and exit
  --model {mobilenet_v2_ssd,mobilenet_v3_ssd,mobilenet_v3_small_ssd}
                        Object detection model (default: 'mobilenet_v2_ssd').
  --random_seed RANDOM_SEED
                        Random seed (default: 3).
  --valid_id VALID_ID   Folder ID to use for validation (default: 49).
  --num_workers NUM_WORKERS
  --no_print
  --eval_freq EVAL_FREQ
                        Frequency to evaluate the model (default: 5, for every 5 epoch).
  --eval_on_valid       Evaluation is performed on the validation set (synthetic images).
  --eval_on_test        Evaluation is performed on the test set (real images).
  --batch_size BATCH_SIZE
                        Batch size (default: 16).
  --epochs EPOCHS       Number of training epochs (default: 75).
  --lr LR               Learning rate (default: 0.05).
  --momentum MOMENTUM   Momentum (default: 0.9).
  --weight_decay WEIGHT_DECAY
                        Weight decay (default: 0.000012).
```
Training uses the proposed augmentation method (aug2).

# Citation
If you use this code in your research, please cite the paper:

```
@inproceedings{cohen2021training,
  title={Training An Embedded Object Detector For Industrial Settings Without Real Images},
  author={Cohen, Julia and Crispim-Junior, Carlos and Chiappa, Jean-Marc and Tougne, Laure},
  booktitle={2021 IEEE International Conference on Image Processing (ICIP)},
  pages={714--718},
  year={2021},
  organization={IEEE}
}
```

# Acknowledgements
This repository is based on the following works:
- MobileNet-V3 with ImageNet pretraining: [https://github.com/d-li14/mobilenetv3.pytorch](https://github.com/d-li14/mobilenetv3.pytorch)
- SSD: [https://github.com/sgrvinod/a-PyTorch-Tutorial-to-Object-Detection](https://github.com/sgrvinod/a-PyTorch-Tutorial-to-Object-Detection)
- MobileNet-V2 - SSD: [https://github.com/qfgaohao/pytorch-ssd](https://github.com/qfgaohao/pytorch-ssd)
- MobileNet-V3 - SSD: [https://github.com/tongyuhome/MobileNetV3-SSD](https://github.com/tongyuhome/MobileNetV3-SSD)

This work is supported by grant CIFRE n.2018/0872 from ANRT.  
<div align="center">
<img src="data/images/LogoLIRIS.jpg" alt="LIRIS logo" height="120" width="120"/>
<img src="data/images/LogoDEMS.png" alt="DEMS logo" height="80" width="80"/>
</div>

