from torchvision.models.detection import maskrcnn_resnet50_fpn
from torchvision.models import mobilenet_v2
import torch

from .mobilenet_v3 import MobileNetV3
from .mobilenet_v2_ssd_lite import mobilenet_v2_ssd_lite
from .mobilenet_v3_ssd_lite import mobilenet_v3_ssd_lite
from . import mobilenetv2_ssd_config, mobilenetv3_ssd_config
from .box_utils import generate_ssd_priors
from .ssd import MatchPrior
from ..config import WEIGHTS_DIR
from .multibox_loss import MultiboxLoss


def create_model(opt):
    num_classes_with_bkgd = opt.num_classes + 1
    if opt.model == 'mobilenet_v2_ssd':
        backbone = mobilenet_v2(pretrained=True)
        priors = generate_ssd_priors(mobilenetv2_ssd_config.specs, opt.input_size)
        model = mobilenet_v2_ssd_lite(num_classes=num_classes_with_bkgd,
                                      base_net=backbone,
                                      is_test=False,
                                      config=mobilenetv2_ssd_config,
                                      priors=priors,
                                      device=opt.device
                                      )
        opt.priors = priors
        opt.model_config = mobilenetv2_ssd_config
        if opt.train:
            opt.target_tf = MatchPrior(opt.priors,
                                       mobilenetv3_ssd_config.center_variance,
                                       mobilenetv3_ssd_config.size_variance,
                                       mobilenetv3_ssd_config.iou_threshold
                                       )
        else:
            opt.target_tf = None
    elif 'mobilenet_v3' in opt.model:
        if 'small' in opt.model:
            backbone = MobileNetV3(mode='small')
            weights_path = WEIGHTS_DIR / 'imagenet-mobilenetv3-small.pth'
        else:
            backbone = MobileNetV3(mode='large')
            weights_path = WEIGHTS_DIR / 'imagenet-mobilenetv3-large.pth'

        if opt.train:
            # Load ImageNet pretrained weights
            state = torch.load(weights_path)
            copy_state = dict()

            for k in state.keys():
                if k.startswith("classifier"):
                    # remove classifier weights (trained for 1000 classes)
                    continue
                copy_state[k] = state[k]

            backbone.load_state_dict(copy_state, strict=False)

        priors = generate_ssd_priors(mobilenetv3_ssd_config.specs, opt.input_size)
        model = mobilenet_v3_ssd_lite(num_classes=num_classes_with_bkgd,
                                      base_net=backbone,
                                      is_test=False,
                                      config=mobilenetv3_ssd_config,
                                      priors=priors,
                                      device=opt.device
                                      )
        opt.priors = priors
        opt.model_config = mobilenetv3_ssd_config
        if opt.train:
            opt.target_tf = MatchPrior(opt.priors,
                                       mobilenetv3_ssd_config.center_variance,
                                       mobilenetv3_ssd_config.size_variance,
                                       mobilenetv3_ssd_config.iou_threshold
                                       )
        else:
            opt.target_tf = None
    elif opt.model == 'mask_rcnn':
        assert not opt.train, "Mask R-CNN can only be used in inference mode, not training."
        model = maskrcnn_resnet50_fpn(pretrained=False,
                                      pretrained_backbone=False,
                                      num_classes=num_classes_with_bkgd,
                                      box_score_thresh=0.1)
        opt.priors = None
    else:
        raise ValueError(f"Model {opt.model} is not available.")
    return model
