"""
Source: https://github.com/shaoshengsong/MobileNetV3-SSD/blob/0ac9f36cff59c2286cf7555da7719c54d3c88c2c/vision/nn/multibox_loss.py
"""

import torch.nn as nn
import torch
import torch.nn.functional as F

from .box_utils import hard_negative_mining


class MultiboxLoss(nn.Module):
    def __init__(self, priors, neg_pos_ratio, center_variance,
                 size_variance, device, iou_threshold=0.5):
        """Implement SSD Multibox Loss.

        Basically, Multibox loss combines classification loss and Smooth L1 regression loss.
        """
        super(MultiboxLoss, self).__init__()
        self.iou_threshold = iou_threshold
        self.neg_pos_ratio = neg_pos_ratio
        self.center_variance = center_variance
        self.size_variance = size_variance
        self.priors = priors
        self.priors.to(device)

    def forward(self, predicted_locations, confidence, gt_locations, labels):
        """Compute classification loss and smooth l1 loss.

        Args:
            confidence (batch_size, num_priors, num_classes): class predictions.
            predicted_locations (batch_size, num_priors, 4): predicted locations.
            labels (batch_size, num_priors): real labels of all the priors.
            gt_locations (batch_size, num_priors, 4): real boxes corresponding all the priors.
        """
        num_classes = confidence.size(2)
        with torch.no_grad():
            # derived from cross_entropy=sum(log(p))
            loss = -F.log_softmax(confidence, dim=2)
            loss = loss[:, :, 0]
            mask = hard_negative_mining(loss, labels, self.neg_pos_ratio)

        confidence = confidence[mask, :]
        classification_loss = F.cross_entropy(confidence.reshape(-1, num_classes), labels[mask], reduction='sum')
        pos_mask = labels > 0
        predicted_locations = predicted_locations[pos_mask, :].reshape(-1, 4)
        gt_locations = gt_locations[pos_mask, :].reshape(-1, 4)
        smooth_l1_loss = F.smooth_l1_loss(predicted_locations, gt_locations, reduction='sum')
        num_pos = gt_locations.size(0)
        if num_pos != 0:
            reg_loss = smooth_l1_loss/num_pos
            classif_loss = classification_loss/num_pos
        else:
            reg_loss = 0
            classif_loss = 0
        return reg_loss, classif_loss
