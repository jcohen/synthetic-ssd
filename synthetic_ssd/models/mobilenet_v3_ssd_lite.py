'''
Adapted from https://github.com/tongyuhome/MobileNetV3-SSD/blob/master/mobilenet_v3_ssd_lite.py
'''
import torch
from torch.nn import ModuleList, Conv2d

from .mobilenet_v3 import MobileNetV3
from .ssd import GraphPath, InvertedResidual, SeparableConv2d, SSD


def mobilenet_v3_ssd_lite(num_classes, base_net, width_mult=1.0,
                            is_test=False, config=None, dropout=0.8, priors=None,
                            device=None):
    """
    Same parameters as MobileNetV3 class init function
    num_classes counts the background class
    :return: Model
    """
    assert isinstance(base_net, MobileNetV3)
    if base_net.get_mode() == 'small':
        source_layer_indexes = [
            GraphPath(9, 'conv', 3),
            13
        ]
        # Reorganize layers
        modules = [x for x in base_net.features]
        modules.append(base_net.conv[:])  # Conv2d + BN + HS + SEmodules.append(base_net.avgpool[0])  # avgpool without HS
        sequence = torch.nn.Sequential(*modules)
        base_net.features = sequence
        base_net.conv = torch.nn.Sequential()

        extras = ModuleList([
            InvertedResidual(576, 512, stride=2, expand_ratio=0.2),
            InvertedResidual(512, 256, stride=2, expand_ratio=0.25),
            InvertedResidual(256, 256, stride=2, expand_ratio=0.5),
            InvertedResidual(256, 64, stride=2, expand_ratio=0.25)
        ])

        regression_headers = ModuleList([
            SeparableConv2d(in_channels=round(288 * width_mult), out_channels=6 * 4,
                            kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=576, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=512, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=256, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=256, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            Conv2d(in_channels=64, out_channels=6 * 4, kernel_size=1),
        ])

        classification_headers = ModuleList([
            SeparableConv2d(in_channels=round(288 * width_mult), out_channels=6 * num_classes, kernel_size=3,
                            padding=1),
            SeparableConv2d(in_channels=576, out_channels=6 * num_classes, kernel_size=3, padding=1),
            SeparableConv2d(in_channels=512, out_channels=6 * num_classes, kernel_size=3, padding=1),
            SeparableConv2d(in_channels=256, out_channels=6 * num_classes, kernel_size=3, padding=1),
            SeparableConv2d(in_channels=256, out_channels=6 * num_classes, kernel_size=3, padding=1),
            Conv2d(in_channels=64, out_channels=6 * num_classes, kernel_size=1),
        ])

    elif base_net.get_mode() == 'large':
        source_layer_indexes = [
            GraphPath(13, 'conv', 3),
            17
        ]
        # Reorganize layers
        modules = [x for x in base_net.features]
        modules.append(base_net.conv[0])  # Conv2d + BN + HS
        sequence = torch.nn.Sequential(*modules)
        base_net.features = sequence
        base_net.conv = torch.nn.Sequential()

        extras = ModuleList([
            InvertedResidual(960, 512, stride=2, expand_ratio=0.2),
            InvertedResidual(512, 256, stride=2, expand_ratio=0.25),
            InvertedResidual(256, 256, stride=2, expand_ratio=0.5),
            InvertedResidual(256, 64, stride=2, expand_ratio=0.25)
        ])

        regression_headers = ModuleList([
            SeparableConv2d(in_channels=round(672 * width_mult), out_channels=6 * 4,
                            kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=960, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=512, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=256, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            SeparableConv2d(in_channels=256, out_channels=6 * 4, kernel_size=3, padding=1, onnx_compatible=False),
            Conv2d(in_channels=64, out_channels=6 * 4, kernel_size=1),
        ])

        classification_headers = ModuleList([
            SeparableConv2d(in_channels=round(672 * width_mult), out_channels=6 * num_classes, kernel_size=3,
                            padding=1),
            SeparableConv2d(in_channels=960, out_channels=6 * num_classes, kernel_size=3, padding=1),
            SeparableConv2d(in_channels=512, out_channels=6 * num_classes, kernel_size=3, padding=1),
            SeparableConv2d(in_channels=256, out_channels=6 * num_classes, kernel_size=3, padding=1),
            SeparableConv2d(in_channels=256, out_channels=6 * num_classes, kernel_size=3, padding=1),
            Conv2d(in_channels=64, out_channels=6 * num_classes, kernel_size=1),
        ])
    else:
        print("Mode {mode} is not available, use 'small' or 'large'.".format(mode=base_net.get_mode()))
        exit(1)

    return SSD(num_classes, base_net, source_layer_indexes, extras,
               classification_headers, regression_headers, is_test=is_test,
               config=config, priors=priors, device=device)
