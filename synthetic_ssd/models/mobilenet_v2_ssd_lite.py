"""
Adapted from https://github.com/sgrvinod/a-PyTorch-Tutorial-to-Object-Detection/blob/master/model.py
and https://github.com/qfgaohao/pytorch-ssd/blob/master/vision/ssd/mobilenet_v2_ssd_lite.py

"""
from torch.nn import ModuleList, Conv2d

from .ssd import GraphPath, InvertedResidual, SeparableConv2d, SSD


def mobilenet_v2_ssd_lite(num_classes, base_net, width_mult=1.0,
                            is_test=False, config=None, priors=None, device=None):

    source_layer_indexes = [
        GraphPath(14, 'conv', 1),
        19
    ]

    extras = ModuleList([
        InvertedResidual(1280, 512, stride=2, expand_ratio=0.2),
        InvertedResidual(512, 256, stride=2, expand_ratio=0.25),
        InvertedResidual(256, 256, stride=2, expand_ratio=0.5),
        InvertedResidual(256, 64, stride=2, expand_ratio=0.25)
    ])

    regression_headers = ModuleList([
        SeparableConv2d(in_channels=round(576 * width_mult), out_channels=6 * 4,
                        kernel_size=3, padding=1, onnx_compatible=False),
        SeparableConv2d(in_channels=1280, out_channels=6 * 4, kernel_size=3,
                        padding=1, onnx_compatible=False),
        SeparableConv2d(in_channels=512, out_channels=6 * 4, kernel_size=3,
                        padding=1, onnx_compatible=False),
        SeparableConv2d(in_channels=256, out_channels=6 * 4, kernel_size=3,
                        padding=1, onnx_compatible=False),
        SeparableConv2d(in_channels=256, out_channels=6 * 4, kernel_size=3,
                        padding=1, onnx_compatible=False),
        Conv2d(in_channels=64, out_channels=6 * 4, kernel_size=1),
    ])

    classification_headers = ModuleList([
        SeparableConv2d(in_channels=round(576 * width_mult), out_channels=6 * num_classes, kernel_size=3, padding=1),
        SeparableConv2d(in_channels=1280, out_channels=6 * num_classes, kernel_size=3, padding=1),
        SeparableConv2d(in_channels=512, out_channels=6 * num_classes, kernel_size=3, padding=1),
        SeparableConv2d(in_channels=256, out_channels=6 * num_classes, kernel_size=3, padding=1),
        SeparableConv2d(in_channels=256, out_channels=6 * num_classes, kernel_size=3, padding=1),
        Conv2d(in_channels=64, out_channels=6 * num_classes, kernel_size=1),
    ])

    return SSD(num_classes, base_net, source_layer_indexes, extras,
               classification_headers, regression_headers, is_test=is_test,
               config=config, priors=priors, device=device)
