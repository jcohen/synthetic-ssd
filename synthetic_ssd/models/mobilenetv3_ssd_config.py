from .box_utils import SSDSpec, SSDBoxSizes

iou_threshold = 0.5
center_variance = 0.1
size_variance = 0.2
neg_pos_ratio = 3

specs = [
    SSDSpec(14, 16, SSDBoxSizes(60, 105), [2, 3]),
    SSDSpec(7, 32, SSDBoxSizes(105, 150), [2, 3]),
    SSDSpec(4, 64, SSDBoxSizes(150, 195), [2, 3]),
    SSDSpec(2, 100, SSDBoxSizes(195, 240), [2, 3]),
    SSDSpec(1, 150, SSDBoxSizes(240, 285), [2, 3]),
    SSDSpec(1, 300, SSDBoxSizes(285, 330), [2, 3])
]

