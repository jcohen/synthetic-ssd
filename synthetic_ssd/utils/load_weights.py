import torch

from synthetic_ssd.config import WEIGHTS_DIR, MASK_RCNN_PATH


def load_weights(model, model_name, tf, weights=None):
    if model_name == "mask_rcnn":
        model = load_mask_rcnn_weights(model, weights=weights)
    elif model_name == "mobilenet_v2_ssd":
        model = load_mobilenet_v2_ssd_weights(model, tf, weights=weights)
    elif model_name == "mobilenet_v3_ssd":
        model = load_mobilenet_v3_ssd_weights(model, tf, weights=weights)
    elif model_name == "mobilenet_v3_small_ssd":
        model = load_mobilenet_v3_small_ssd_weights(model, tf, weights=weights)
    return model


def load_mask_rcnn_weights(model, weights=None):
    weights = MASK_RCNN_PATH
    state_dict = torch.load(weights)
    model.load_state_dict(state_dict['state_dict'])
    return model


def load_mobilenet_v2_ssd_weights(model, tf, weights=None):
    if weights is not None:
        weights = WEIGHTS_DIR / tf / "tless_icip21_V2ssd.pth"
    state_dict = torch.load(weights)
    model.load_state_dict(state_dict)
    return model


def load_mobilenet_v3_ssd_weights(model, tf, weights=None):
    if weights is not None:
        weights = WEIGHTS_DIR / tf / "tless_icip21_V3ssd.pth"
    state_dict = torch.load(weights)
    model.load_state_dict(state_dict, strict=False)
    return model


def load_mobilenet_v3_small_ssd_weights(model, tf, weights=None):
    if weights is not None:
        weights = WEIGHTS_DIR / tf / "tless_icip21_V3smallssd.pth"
    state_dict = torch.load(weights)
    model.load_state_dict(state_dict, strict=False)
    return model
