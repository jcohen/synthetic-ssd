import torch


def initialize(opt):
    torch.manual_seed(opt.random_seed)
    torch.backends.cudnn.deterministic = True
    device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
    opt.device = device
    opt.k_rgb = 3
