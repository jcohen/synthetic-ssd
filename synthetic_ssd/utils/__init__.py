import argparse
import os


def parse_arguments():
    args = argparse.ArgumentParser()
    return args.parse_args()


if __name__ == "__main__":
    opt = parse_arguments()
