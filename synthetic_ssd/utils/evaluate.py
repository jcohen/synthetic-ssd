from torchvision.ops import nms


def custom_nms(boxes, confidences, images_names, score_threshold=0.1, iou_threshold=0.5):
    """
    :param boxes: tensor of size(batch_size, N_boxes, 4).
    :param confidences: tensor of size (batch_size, N_boxes, Nclasses+1), with background.
    :param images_names: list of length batch_size.
    :param score_threshold: remove boxes with confidence score < score_threshold.
    :param iou_threshold: remove boxes with IoU with other box of higher confidence > iou_threshold.
    :return: boxes, labels, confidences, images names as lists.
    """
    out_boxes = []
    out_labels = []
    out_confidences = []
    out_names = []

    # Remove the background class
    confidences = confidences[:, :, 1:]

    # For each image in the batch:
    for i in range(boxes.size(0)):
        im_name = images_names[i]
        # For each class
        for cls in range(confidences.size(-1)):
            # /!\ Class label starts at 1 in the ground truth, 0 here /!\
            conf_mask = confidences[i, :, cls] > score_threshold
            boxes_im = boxes[i, conf_mask, :]
            scores_im = confidences[i, conf_mask, cls]

            if scores_im.size(0) > 0:
                keep = nms(boxes_im, scores_im, iou_threshold=iou_threshold)
            else:
                keep = []

            for ind in keep:
                out_boxes.append(boxes_im[ind.item()])
                out_labels.append(cls+1)
                out_confidences.append(scores_im[ind.item()])
                out_names.append(im_name)

    return out_boxes, out_labels, out_confidences, out_names