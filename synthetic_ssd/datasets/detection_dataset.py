import os
from pathlib import Path

import json
import cv2
import torch
from torch.utils.data import Dataset
import albumentations as A

from synthetic_ssd.config import TLESS_TRAIN_PATH, TLESS_TEST_PATH


class TLESSDetection(Dataset):
    def __init__(self, tf, target_tf, mode):
        self.tf = tf
        self.target_tf = target_tf

        if mode == "train":
            path = Path(TLESS_TRAIN_PATH)
        else:
            path = Path(TLESS_TEST_PATH)
        assert path.exists(), f"Path {path} does not exist."

        self.rgb_paths = list()
        self.gt = dict()
        self.gt['boxes'] = dict()
        self.gt['labels'] = dict()
        annot_gt = None
        for cur_path in path.iterdir():
            if not cur_path.is_dir():
                continue
            images = [str(f) for f in (cur_path / 'rgb').iterdir()
                      if f.name.endswith(".jpg") or f.name.endswith(".png")]
            self.rgb_paths.extend(images)

            annot_gt = json.loads((cur_path / 'scene_gt.json').read_text())
            annot_gt_info = json.loads((cur_path / 'scene_gt_info.json').read_text())
            self.gt['boxes'][cur_path.name.zfill(6)] = dict()
            self.gt['labels'][cur_path.name.zfill(6)] = dict()
            for image_id in annot_gt.keys():
                boxes = list()
                labels = list()
                for id_data, bbox_data in zip(annot_gt[image_id], annot_gt_info[image_id]):
                    x, y, w, h = bbox_data["bbox_visib"]  # left, top, width, height
                    if x == -1 and y == -1 and w == -1 and h == -1:
                        continue
                    if bbox_data["px_count_visib"] < 100 or w < 10 or h < 10:
                        continue
                    boxes.append([x, y, x + w, y + h])
                    labels.append(id_data["obj_id"])
                self.gt['boxes'][cur_path.name.zfill(6)][image_id.zfill(6)] = boxes
                self.gt['labels'][cur_path.name.zfill(6)][image_id.zfill(6)] = labels
        assert annot_gt is not None, f"Path {path} to dataset seems incorrect, got 0 annotations."
        del annot_gt, annot_gt_info

        self.length = len(self.rgb_paths)
        self.classes = list(range(1, 31))

    def __len__(self):
        return self.length

    def __repr__(self):
        return f"Dataset T-LESS of RGB images for object detection: "

    def __getitem__(self, idx):
        color_img = cv2.imread(self.rgb_paths[idx])[:, :, :3]  # uint8, BGR
        color_img = cv2.cvtColor(color_img, cv2.COLOR_BGR2RGB)

        full_path = self.rgb_paths[idx]
        list_path = full_path.split(os.sep)
        folder_id = list_path[-3]
        image_id = list_path[-1].split(".")[0]
        target = dict()
        target["boxes"] = self.gt["boxes"][folder_id][image_id]
        target["labels"] = self.gt["labels"][folder_id][image_id]
        target['image_name'] = full_path
        target['image_size'] = color_img.shape[:2]

        # Pre-process and augment data
        target["boxes"] = normalize_bboxes(target["boxes"], target["image_size"][0], target["image_size"][1])
        len_new_boxes = 0
        # Make sure the image still has boxes after augmentations
        while not len_new_boxes:
            transformed = self.tf(image=color_img, bboxes=target["boxes"], labels=target["labels"])
            len_new_boxes = len(transformed["bboxes"])
        color_img = transformed["image"]
        target["boxes"] = torch.as_tensor(transformed["bboxes"], dtype=torch.float32)
        target["labels"] = torch.as_tensor(transformed["labels"], dtype=torch.int64)

        target['raw_boxes'], target["raw_labels"] = target["boxes"], target["labels"]
        if self.target_tf:
            target["boxes"], target["labels"] = self.target_tf(target["boxes"], target["labels"])

        if color_img.dtype == torch.uint8:
            color_img = color_img.type(torch.FloatTensor)

        return color_img, target

    def train_valid_indices(self, folder_id=49):
        """
        Divides the TLESS synthetic dataset into a training and validation subsets.
        :param id: (int) number identifying the folder to use as validation data (last folder 49 by default).
        :return: (tuple) pair of lists corresponding to the indices of the training and validation sets respectively.
        """
        train_ind = list()
        valid_ind = list()
        for ind, file in enumerate(self.rgb_paths):
            path, _ = os.path.split(file)
            if str(folder_id) in path:
                valid_ind.append(ind)
            else:
                train_ind.append(ind)
        return train_ind, valid_ind


def normalize_bboxes(bboxes, im_height, im_width):
    """
    Normalize bounding boxes.
    :param bboxes: a sequence of bounding boxes with format x1y1x2y2
    :param im_height: image height
    :param im_width: image width
    :return: the normalized bounding boxes, with format "albumentations"
    """
    return A.normalize_bboxes(bboxes, rows=im_height, cols=im_width)


def collate_fn(batch):
    """
    Collate target data stored in a dictionary.
    :param batch:
    :return:
    """
    images = [b[0] for b in batch]
    images = torch.stack(images, dim=0)

    targets = [b[1] for b in batch]

    return images, targets
