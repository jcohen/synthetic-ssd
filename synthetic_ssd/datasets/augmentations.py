import albumentations as A
from albumentations.pytorch import ToTensorV2


def train_tf(out_size=224):
    """
    Proposed extensive augmentation pipeline.
    :param out_size:
    :return:
    """
    tf = A.Compose([
        A.OneOf([
            A.ColorJitter(p=0.5, brightness=(0.5, 1.4), contrast=0.5, saturation=0.9, hue=0.5),
            A.ColorJitter(p=0.5, brightness=0, contrast=0, saturation=0, hue=0.5),
            A.ColorJitter(p=0.5, brightness=0, contrast=0, saturation=0.9, hue=0),
            A.ColorJitter(p=0.5, brightness=0, contrast=0.5, saturation=0, hue=0),
            A.ColorJitter(p=0.5, brightness=(0.5, 1.4), contrast=0, saturation=0, hue=0),
        ], p=0.8),

        A.CLAHE(p=0.5),

        A.RGBShift(p=0.5),

        A.OneOf([
            A.Blur(p=0.5, blur_limit=(3, 7)),
            A.GaussianBlur(p=0.5, blur_limit=(3, 7), sigma_limit=0),
            A.MedianBlur(p=0.5, blur_limit=(3, 7)),
            A.MotionBlur(p=0.5, blur_limit=(5, 15)),
        ], p=0.5),

        A.GaussNoise(p=0.8),
        A.MultiplicativeNoise(p=0.2, multiplier=(0.7, 1.3)),
        A.ISONoise(p=0.2),

        A.VerticalFlip(p=0.5),

        A.RandomCrop(p=0.6, height=400, width=400),

        A.Resize(p=1.0, height=out_size, width=out_size),
        A.Normalize(p=1.0),
        ToTensorV2(p=1.0)
    ],
        bbox_params=A.BboxParams(format="albumentations",
                                 label_fields=['labels'],
                                 min_area=250,
                                 )
    )
    return tf


def test_tf_ssd(out_size=224):
    tf = A.Compose(
        [
            A.Resize(p=1.0, height=out_size, width=out_size),
            A.Normalize(p=1.0),
            ToTensorV2(p=1.0)
        ],
        bbox_params=A.BboxParams(format="albumentations",
                                 label_fields=['labels'])
    )
    return tf


def test_tf_rcnn():
    tf = A.Compose(
        [
            ToTensorV2(p=1.0)
        ],
        bbox_params=A.BboxParams(format="albumentations",
                                 label_fields=['labels'])
    )
    return tf
