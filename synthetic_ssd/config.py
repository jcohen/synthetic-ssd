import synthetic_ssd
from pathlib import Path

PROJECT_ROOT = Path(synthetic_ssd.__file__).parent.parent
PROJECT_DIR = PROJECT_ROOT
DATA_DIR = PROJECT_DIR / 'data'
WEIGHTS_DIR = DATA_DIR / 'weights'
MASK_RCNN_PATH = WEIGHTS_DIR / "maskrcnn_detector-bop-tless-pbr--873074.pth"

TLESS_BASE_PATH = DATA_DIR / 'tless'
TLESS_TRAIN_PATH = TLESS_BASE_PATH / 'train_pbr'
TLESS_TEST_PATH = TLESS_BASE_PATH / 'test_primesense'

DEPS_DIR = PROJECT_DIR / 'deps'

SAVE_DIR = DATA_DIR / 'output'
