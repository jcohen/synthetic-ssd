import argparse
import math
import pprint

import tqdm
import torch
from torch.utils.data import DataLoader, Subset

from synthetic_ssd.utils.configuration import initialize
from synthetic_ssd.utils.evaluate import custom_nms
from synthetic_ssd.models import create_model, MultiboxLoss
from synthetic_ssd.datasets.detection_dataset import TLESSDetection, collate_fn
from synthetic_ssd.datasets.augmentations import train_tf, test_tf_ssd
from synthetic_ssd.config import SAVE_DIR

from synthetic_ssd.metrics import _init_paths
from BoundingBox import BoundingBox
from BoundingBoxes import BoundingBoxes
from Evaluator import Evaluator
import utils as metrics_utils


def main():
    args = argparse.ArgumentParser()
    args.add_argument('--model', type=str, default='mobilenet_v2_ssd',
                      choices=['mobilenet_v2_ssd', 'mobilenet_v3_ssd', 'mobilenet_v3_small_ssd'],
                      help="Object detection model (default: 'mobilenet_v2_ssd').")
    args.add_argument('--tf', type=str, default='aug2', choices=['aug1', 'aug2'],
                      help="Augmentation method to applied for training (default:'aug2').")
    args.add_argument('--random_seed', type=int, default=3,
                      help="Random seed (default: 3).")
    args.add_argument('--valid_id', type=int, default=49,
                      help="Folder ID to use for validation (default: 49).")
    args.add_argument("--num_workers", type=int, default=2)
    args.add_argument("--no_print", action="store_true",
                      help="Flag to disable printing. This will also prevent the evaluation step from running.")
    args.add_argument("--eval_freq", type=int, default=5,
                      help="Frequency to evaluate the model (default: 5, for every 5 epoch).")
    args.add_argument("--eval_on_valid", action="store_true",
                      help="Evaluation is performed on the validation set (synthetic images).")
    args.add_argument("--eval_on_test", action="store_true",
                      help="Evaluation is performed on the test set (real images).")
    args.add_argument("--save_freq", type=int, default=0,
                      help="Frequency to save the trained model (default: 0, only save model at the end).")
    # Training parameters
    args.add_argument('--batch_size', type=int, default=16,
                      help="Batch size (default: 16).")
    args.add_argument('--epochs', type=int, default=75,
                      help="Number of training epochs (default: 75).")
    args.add_argument('--lr', type=float, default=0.05,
                      help="Learning rate (default: 0.05).")
    args.add_argument('--momentum', type=float, default=0.9,
                      help="Momentum (default: 0.9).")
    args.add_argument('--weight_decay', type=float, default=0.000012,
                      help="Weight decay (default: 0.000012).")

    opt = args.parse_args()
    opt.train = True
    opt.num_classes = 30
    opt.input_size = 224
    run_training(opt)


def run_training(opt):
    initialize(opt)

    if not SAVE_DIR.exists():
        SAVE_DIR.mkdir(parents=True)

    # Load model
    model = create_model(opt)  # Also adds target_tf, priors and model config file into opt

    # Load datasets
    tf = train_tf(opt.input_size)
    dataset = TLESSDetection(tf=tf, target_tf=opt.target_tf, mode='train')
    train_ind, valid_ind = dataset.train_valid_indices(folder_id=opt.valid_id)

    train_dataset = Subset(dataset, train_ind)
    train_loader = DataLoader(train_dataset,
                              batch_size=opt.batch_size,
                              shuffle=True,
                              num_workers=opt.num_workers,
                              collate_fn=collate_fn)

    if opt.eval_on_valid:
        valid_dataset = Subset(dataset, valid_ind)
        valid_loader = DataLoader(valid_dataset,
                                  batch_size=1,
                                  shuffle=False,
                                  num_workers=1,
                                  collate_fn=collate_fn)
    if opt.eval_on_test:
        test_dataset = TLESSDetection(tf=test_tf_ssd(opt.input_size), target_tf=None, mode="test")
        test_loader = DataLoader(test_dataset,
                                 batch_size=1,
                                 shuffle=False,
                                 num_workers=1,
                                 collate_fn=collate_fn)

    if not opt.no_print:
        print(model)
        print(dataset)
        pprint.pprint(opt)

    model.to(opt.device)
    optimizer = torch.optim.SGD(model.parameters(),
                                lr=opt.lr,
                                momentum=opt.momentum,
                                weight_decay=opt.weight_decay)

    criterion = MultiboxLoss(opt.priors,
                             iou_threshold=opt.model_config.iou_threshold,
                             neg_pos_ratio=3,
                             center_variance=opt.model_config.center_variance,
                             size_variance=opt.model_config.size_variance,
                             device=opt.device)
    try:
        for epoch in range(opt.epochs):
            print(f"\nTraining epoch {epoch+1}/{opt.epochs}")

            model.train(True)
            model.is_test = False
            running_loss = 0.0
            running_regression_loss = 0.0
            running_classification_loss = 0.0

            num = 0
            optimizer.zero_grad()

            for data in tqdm.tqdm(train_loader):
                images, targets = data
                boxes = [t['boxes'] for t in targets]
                labels = [t['labels'] for t in targets]
                if len(boxes) > 0:
                    boxes = torch.stack(boxes, dim=0).to(opt.device)
                    labels = torch.stack(labels, dim=0).to(opt.device)

                confidences, locations = model(images.to(opt.device))
                regression_loss, classification_loss = criterion(locations, confidences, boxes, labels)

                loss = regression_loss + classification_loss
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()

                running_loss += loss.item()
                running_regression_loss += regression_loss.item()
                running_classification_loss += classification_loss.item()

                if not math.isfinite(loss.item()):
                    print(f"Loss is {loss.item()}, stopping training")
                    exit(1)

                num += images.size(0)
            running_loss /= num
            running_regression_loss /= num
            running_classification_loss /= num
            if not opt.no_print:
                print(f"\nRunning loss: {running_loss} ({running_classification_loss}"
                      f" classification, {running_regression_loss} regression).\n")

            if opt.save_freq > 0:
                if ((epoch+1) % opt.save_freq) == 0:
                    name = f"{opt.model}_epoch{epoch+1}.pth"
                    torch.save(model.state_dict(), SAVE_DIR / name)

            if ((epoch+1) % opt.eval_freq) == 0 and not opt.no_print:
                if opt.eval_on_valid:
                    print("Evaluation on validation set")
                    run_evaluation(opt, model, valid_loader)
                if opt.eval_on_test:
                    print("Evaluation on test set")
                    run_evaluation(opt, model, test_loader)

    finally:
        print("Done!")
        name = f"{opt.model}_epoch{epoch+1}_last.pth"
        full_path = SAVE_DIR / name
        torch.save(model.state_dict(), full_path)
        print(f"Saved trained model: {full_path}")


@torch.no_grad()
def run_evaluation(opt, model, dataloader):
    model.eval()
    model.is_test = True

    evaluator = Evaluator()
    bboxes = BoundingBoxes()

    for data in tqdm.tqdm(dataloader):
        images, targets = data
        raw_boxes = [t['raw_boxes'] for t in targets]
        raw_labels = [t['raw_labels'] for t in targets]
        image_names = [t['image_name'] for t in targets]
        image_sizes = [t['image_size'] for t in targets]

        for im in range(len(images)):
            im_boxes = raw_boxes[im]  # tensor of size (n_boxes, 4)
            im_labels = raw_labels[im]
            im_name = image_names[im]
            im_size = image_sizes[im]
            for i in range(im_boxes.size(0)):
                x1, y1, x2, y2 = im_boxes[i]
                x = (x1.item()+x2.item())/2.
                y = (y1.item()+y2.item())/2.
                w = x2.item() - x1.item()
                h = y2.item() - y1.item()
                gtBox = BoundingBox(imageName=im_name,
                                    classId=str(im_labels[i].item()),
                                    x=x, y=y,
                                    w=w, h=h,
                                    typeCoordinates=metrics_utils.CoordinatesType.Relative,
                                    imgSize=(im_size[1], im_size[0]),
                                    bbType=metrics_utils.BBType.GroundTruth,
                                    format=metrics_utils.BBFormat.XYWH)
                bboxes.addBoundingBox(gtBox)

        confidences, locations = model(images.to(opt.device))
        out_boxes, out_labels, out_confidences, out_names = custom_nms(locations, confidences,
                                                                       images_names=image_names,
                                                                       iou_threshold=0.5,
                                                                       score_threshold=0.1)

        for i in range(len(out_boxes)):
            im_size = image_sizes[image_names.index(out_names[i])]  # (h, w)
            x1, y1, x2, y2 = out_boxes[i].cpu()
            x = (x1.item()+x2.item())/2.
            y = (y1.item()+y2.item())/2.
            w = x2.item() - x1.item()
            h = y2.item() - y1.item()
            newBox = BoundingBox(imageName=out_names[i],
                                 classId=str(out_labels[i]),
                                 x=x, y=y, w=w, h=h,
                                 typeCoordinates=metrics_utils.CoordinatesType.Relative,
                                 imgSize=(im_size[1], im_size[0]),
                                 bbType=metrics_utils.BBType.Detected,
                                 classConfidence=out_confidences[i],
                                 format=metrics_utils.BBFormat.XYWH)
            bboxes.addBoundingBox(newBox)

    metrics = evaluator.GetPascalVOCMetrics(bboxes)
    AP = []
    for cls_metrics in metrics:
        print("Class {cls}: {AP}% AP, {pos} gt positives, "
              "{TP} true positives, {FP} false positives".
              format(cls=cls_metrics['class'],
                     AP=cls_metrics['AP'] * 100,
                     pos=cls_metrics['total positives'],
                     TP=cls_metrics['total TP'],
                     FP=cls_metrics['total FP']
                     ))
        AP.append(cls_metrics['AP'])
    mean_ap = sum(AP) / len(AP)
    print(f"Average precision: {mean_ap * 100}%")


if __name__ == "__main__":
    main()

