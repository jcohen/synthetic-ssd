from tqdm import tqdm

from torch.utils.data import DataLoader

from synthetic_ssd.datasets.detection_dataset import TLESSDetection, collate_fn
from synthetic_ssd.datasets.augmentations import train_tf, test_tf_ssd, test_tf_rcnn


if __name__ == "__main__":
    for mode in ('train', 'test'):
        for tf in (train_tf(), test_tf_ssd(), test_tf_rcnn()):
            dataset = TLESSDetection(tf=tf, target_tf=None, mode=mode)
            loader = DataLoader(dataset, shuffle=(mode == 'train'),
                                batch_size=2, num_workers=2,
                                drop_last=False, collate_fn=collate_fn)

        for data in tqdm(loader):
            pass
