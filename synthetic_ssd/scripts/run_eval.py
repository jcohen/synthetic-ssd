import argparse

import tqdm
import torch
from torch.utils.data import DataLoader

from synthetic_ssd.utils.configuration import initialize
from synthetic_ssd.utils.evaluate import custom_nms
from synthetic_ssd.utils.load_weights import load_weights
from synthetic_ssd.models import create_model
from synthetic_ssd.datasets.detection_dataset import TLESSDetection, collate_fn
from synthetic_ssd.datasets.augmentations import test_tf_ssd, test_tf_rcnn

from synthetic_ssd.metrics import _init_paths
from BoundingBox import BoundingBox
from BoundingBoxes import BoundingBoxes
from Evaluator import Evaluator
import utils as metrics_utils


def main():
    args = argparse.ArgumentParser()
    args.add_argument('--model', type=str, default='mobilenet_v2_ssd',
                      choices=['mobilenet_v2_ssd', 'mobilenet_v3_ssd', 'mobilenet_v3_small_ssd', 'mask_rcnn'],
                      help="Object detection model (default: 'mobilenet_v2_ssd').")
    args.add_argument('--tf', type=str, default='aug2', choices=['aug1', 'aug2'],
                      help="Augmentation method to applied for training (default:'aug2').")
    args.add_argument('--random_seed', type=int, default=3,
                      help="Random seed (default: 3).")
    args.add_argument('--weights', type=str,
                      help="Path to the weights file to evaluate.")
    opt = args.parse_args()
    opt.train = False
    opt.num_classes = 30
    opt.input_size = 224
    run_evaluation(opt)


@torch.no_grad()
def run_evaluation(opt):
    initialize(opt)

    # Load dataset
    if "mobilenet" in opt.model:
        tf = test_tf_ssd(opt.input_size)
    else:
        tf = test_tf_rcnn()
    dataset = TLESSDetection(tf=tf, target_tf=None, mode='test')

    # Load model
    model = create_model(opt)
    model = load_weights(model, model_name=opt.model, tf=opt.tf, weights=opt.weights)

    model.to(opt.device)
    model.eval()
    if hasattr(model, 'is_test'):
        model.is_test = True

    # Eval loop
    evaluator = Evaluator()
    if opt.model == "mask_rcnn":
        bboxes = test_mask_rcnn(opt, dataset, model)
    else:
        dataloader = DataLoader(dataset,
                                batch_size=1,
                                shuffle=False,
                                num_workers=2,
                                collate_fn=collate_fn)

        bboxes = BoundingBoxes()

        for data in tqdm.tqdm(dataloader):
            images, targets = data
            raw_boxes = [t['raw_boxes'] for t in targets]
            raw_labels = [t['raw_labels'] for t in targets]
            image_names = [t['image_name'] for t in targets]
            image_sizes = [t['image_size'] for t in targets]

            for im in range(len(images)):
                im_boxes = raw_boxes[im]  # tensor of size (n_boxes, 4)
                im_labels = raw_labels[im]
                im_name = image_names[im]
                im_size = image_sizes[im]
                for i in range(im_boxes.size(0)):
                    x1, y1, x2, y2 = im_boxes[i]
                    x = (x1.item()+x2.item())/2.
                    y = (y1.item()+y2.item())/2.
                    w = x2.item() - x1.item()
                    h = y2.item() - y1.item()
                    gtBox = BoundingBox(imageName=im_name,
                                        classId=str(im_labels[i].item()),
                                        x=x, y=y,
                                        w=w, h=h,
                                        typeCoordinates=metrics_utils.CoordinatesType.Relative,
                                        imgSize=(im_size[1], im_size[0]),
                                        bbType=metrics_utils.BBType.GroundTruth,
                                        format=metrics_utils.BBFormat.XYWH)
                    bboxes.addBoundingBox(gtBox)

            confidences, locations = model(images.to(opt.device))
            out_boxes, out_labels, out_confidences, out_names = custom_nms(locations, confidences,
                                                                           images_names=image_names,
                                                                           iou_threshold=0.5,
                                                                           score_threshold=0.1)

            for i in range(len(out_boxes)):
                im_size = image_sizes[image_names.index(out_names[i])]  # (h, w)
                x1, y1, x2, y2 = out_boxes[i].cpu()
                x = (x1.item()+x2.item())/2.
                y = (y1.item()+y2.item())/2.
                w = x2.item() - x1.item()
                h = y2.item() - y1.item()
                newBox = BoundingBox(imageName=out_names[i],
                                     classId=str(out_labels[i]),
                                     x=x, y=y, w=w, h=h,
                                     typeCoordinates=metrics_utils.CoordinatesType.Relative,
                                     imgSize=(im_size[1], im_size[0]),
                                     bbType=metrics_utils.BBType.Detected,
                                     classConfidence=out_confidences[i],
                                     format=metrics_utils.BBFormat.XYWH)
                bboxes.addBoundingBox(newBox)

    metrics = evaluator.GetPascalVOCMetrics(bboxes)
    AP = []
    for cls_metrics in metrics:
        print("Class {cls}: {AP}% AP, {pos} gt positives, "
              "{TP} true positives, {FP} false positives".
              format(cls=cls_metrics['class'],
                     AP=cls_metrics['AP'] * 100,
                     pos=cls_metrics['total positives'],
                     TP=cls_metrics['total TP'],
                     FP=cls_metrics['total FP']
                     ))
        AP.append(cls_metrics['AP'])
    mean_ap = sum(AP) / len(AP)
    print(f"Average precision: {mean_ap * 100}%")


def test_mask_rcnn(opt, dataset, model):
    bboxes = BoundingBoxes()
    for img, target in tqdm.tqdm(dataset):
        img = img.div(255.)
        _, height, width = img.shape

        img = [img.to(opt.device)]
        im_boxes = target["raw_boxes"]
        im_labels = target["raw_labels"]
        im_name = target["image_name"]

        for i in range(im_boxes.size(0)):
            x1, y1, x2, y2 = im_boxes[i]
            x = (x1.item()+x2.item())/2.
            y = (y1.item()+y2.item())/2.
            w = x2.item() - x1.item()
            h = y2.item() - y1.item()
            gtBox = BoundingBox(imageName=im_name,
                                classId=str(im_labels[i].item()),
                                x=x, y=y, w=w, h=h,
                                typeCoordinates=metrics_utils.CoordinatesType.Relative,
                                imgSize=(width, height),
                                bbType=metrics_utils.BBType.GroundTruth,
                                format=metrics_utils.BBFormat.XYWH)
            bboxes.addBoundingBox(gtBox)

        with torch.no_grad():
            output = model(img)[0]

        out_boxes = output["boxes"].cpu()
        out_labels = output["labels"].cpu()
        out_confidences = output["scores"].cpu()

        out_boxes[:, 0] = out_boxes[:, 0].div(width)
        out_boxes[:, 2] = out_boxes[:, 2].div(width)
        out_boxes[:, 1] = out_boxes[:, 1].div(height)
        out_boxes[:, 3] = out_boxes[:, 3].div(height)

        score_threshold = 0.1
        for i in range(out_boxes.shape[0]):
            if out_confidences[i] < score_threshold:
                continue

            x1, y1, x2, y2 = out_boxes[i].cpu()
            x = (x1.item()+x2.item())/2.
            y = (y1.item()+y2.item())/2.
            w = x2.item() - x1.item()
            h = y2.item() - y1.item()
            newBox = BoundingBox(imageName=im_name,
                                 classId=str(out_labels[i].item()),
                                 x=x, y=y, w=w, h=h,
                                 typeCoordinates=metrics_utils.CoordinatesType.Relative,
                                 imgSize=(width, height),
                                 bbType=metrics_utils.BBType.Detected,
                                 classConfidence=out_confidences[i],
                                 format=metrics_utils.BBFormat.XYWH)
            if newBox is not None:
                bboxes.addBoundingBox(newBox)

    return bboxes


if __name__ == "__main__":
    main()

